# L'Agence du Bout du Monde

À dos d'escargot jusqu'au bout de votre chéquier...

Projet de fin de formation **Développement Java / Cobol** de l'INTI. Site factice d'agence de voyages.

## Fonctionnalités

- Parcourir des hôtels par destination. Réserver un hôtel entre deux dates.
- Accéder aux commandes à payer et passées d'un client, les payer.
- Plan VIP : un utilisateur VIP gagne des points pour chaque commande payée, qui peuvent être converties en une remise.
- Gestion des rôles : inscription d'un nouveau client, rôles employé et admin avec permissions.
- Back office:
    - Ajouter / modifier des destinations
    - Ajouter / modifier des Hôtels, des Stations balnéaires
    - Ajouter / modifier des activités
    - Ajouter / modifier des utilisateurs / clients / vips
    - Modération des avis

## Dépendances

- java JDK 8
- maven

## Installation et exécution

Dans la racine du projet exécuter la commande

```
mvn spring-boot:run
```

## Jeu de données de base

Le script SQL `src/main/resources/reboot.sql` contient un jeu de données de base pour donner du volume au site.

Tous les utilisateurs créés ont comme mot de passe 123 :
- `admin@admin.a` : droits admin.
- `jojo@gmail.com` : rôle employé.
- `client@gmail.com` : client.
- `vip@gmail.com` : vip.

## Captures d'écran

![Page d'accueil](./assets/images/2024-01-04-10-52-10.png) 

![Menu off canva](./assets/images/2024-01-04-10-55-46.png)

![Carousel destinations et footer](./assets/images/2024-01-04-10-57-51.png)

![Page recherche](./assets/images/2024-01-04-10-52-29.png) 

![Page réservable](./assets/images/2024-01-04-10-52-40.png) 

![Formulaire de trajet](./assets/images/2024-01-04-10-52-55.png) 

![Récapitulatif de commande](./assets/images/2024-01-04-10-54-44.png) 

![Page panier](./assets/images/2024-01-04-10-54-58.png) 

![Page profil](./assets/images/2024-01-04-10-55-55.png) 

![Formulaire d'avis](./assets/images/2024-01-04-10-56-40.png) 

![Back Office avis](./assets/images/2024-01-04-11-10-37.png) 

![Back Office destinations](./assets/images/2024-01-04-11-10-48.png) 

![Page à propos](./assets/images/2024-01-04-10-58-02.png) 
