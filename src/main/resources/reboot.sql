-- Use only to reboot app data
-- 1. Drop entries from all tables
USE ABM;
DELETE FROM activite;
ALTER TABLE activite AUTO_INCREMENT = 1;
DELETE FROM avis;
ALTER TABLE avis AUTO_INCREMENT = 1;
DELETE FROM client;
DELETE FROM commande;
ALTER TABLE commande AUTO_INCREMENT = 1;
DELETE FROM destination;
ALTER TABLE destination AUTO_INCREMENT = 1;
DELETE FROM hotel;
DELETE FROM join_reservable_activite;
DELETE FROM join_user_role;
DELETE FROM panier;
ALTER TABLE panier AUTO_INCREMENT = 1;
DELETE FROM remise;
ALTER TABLE remise AUTO_INCREMENT = 1;
DELETE FROM reservable;
ALTER TABLE reservable AUTO_INCREMENT = 1;
DELETE FROM role;
ALTER TABLE role AUTO_INCREMENT = 1;
DELETE FROM station_balneaire;
DELETE FROM trajet;
ALTER TABLE trajet AUTO_INCREMENT = 1;
DELETE FROM user;
ALTER TABLE user AUTO_INCREMENT = 1;
DELETE FROM vip;

-- 2. destinations
INSERT into destination (prix, ville, image_destination) values 
	(150, 'Paris','https://img.freepik.com/photos-gratuite/paysage-urbain-paris-sous-lumiere-du-soleil-ciel-bleu-fra_181624-50289.jpg'),
	(200, 'Toulouse','https://img.freepik.com/photos-gratuite/vue-du-capitole-hotel-ville-est-administration-municipale-ville-toulouse-france_268835-3652.jpg' ),
	(800, 'New-York', 'https://img.freepik.com/photos-gratuite/central-park-manhattan-new-york-immense-magnifique-parc-entoure-gratte-ciel-etang_181624-50335.jpg'),
	(200, 'Suisse','https://img.freepik.com/photos-gratuite/vaches-paissant-champ-vert-vaches-dans-pres-alpins-beau-paysage-alpin_8353-6471.jpg' ),
	(750, 'La Havane', 'https://cdn.generationvoyage.fr/2021/02/guide-la-havane-1.jpg'),
	(300, 'Cappadoce','https://img.freepik.com/photos-gratuite/vallee-amour-goreme-cappadoce-turquie_335224-1186.jpg' ),
	(400, 'Islande', 'https://img.freepik.com/photos-gratuite/belle-eglise-rouge-village-vik-islande_335224-605.jpg'),
	(320, 'Croatie', 'https://img.freepik.com/photos-gratuite/beaux-paysages-du-juego-tronos-dubrovnik-croatie-pendant-journee_181624-43476.jpg'),
	(220, 'Tunisie', 'https://img.freepik.com/photos-gratuite/fond-culture-maroc-arabe-marocain_1203-5669.jpg'),
	(665, 'Vietnam', 'https://img.freepik.com/photos-gratuite/brume-rurale-pointe-bambou-lijiang_1417-410.jpg');

-- 3. users et roles
 insert into user (email, nom, password, prenom, photo_profil) values 
	('admin@admin.a','admin','$2a$10$5HA3gca4dWgJKDSApQjOHePAI9GpjPbEtg/4gHoiJ5/Niahp7IV4m','admin', 'https://vectorified.com/images/admin-logo-icon-16.jpg'),
	('client@gmail.com','Durand','$2a$10$5HA3gca4dWgJKDSApQjOHePAI9GpjPbEtg/4gHoiJ5/Niahp7IV4m','Paul', 'https://www.artribune.com/wp-content/uploads/2015/08/Paul-Durand-Ruel-ritratto-da-Renoir-nel-1910-480x589.jpg'),
	('vip@gmail.com', 'Albert','$2a$10$5HA3gca4dWgJKDSApQjOHePAI9GpjPbEtg/4gHoiJ5/Niahp7IV4m','Marie', 'http://www.parisartistes.com/wp-content/uploads/2015/06/PORTRAIT-NB-Marie-ALBERT-@-Copyright-Igrec-BD.jpg'),
	('jojo@gmail.com', 'le Terrible', '$2a$10$5HA3gca4dWgJKDSApQjOHePAI9GpjPbEtg/4gHoiJ5/Niahp7IV4m', 'Jojo', 'https://i.ytimg.com/vi/kEaldCpp4zw/hqdefault.jpg');
 
insert into role (role_name) values
	('ROLE_ADMIN'),
	('ROLE_EMPLOYEE'),
	('ROLE_CLIENT');

insert into join_user_role (user_id, role_id) values
	(1, 1),
	(2, 3),
	(3, 3),
	(4, 2);

-- 5. clients + paniers et vips
insert into panier () values  
	(),
	();

insert into client (user_id, panier_id) values 
	(2,1),
	(3,2);

insert into vip (points, user_id) values 
	(400, 3);
  
-- 6. activités
insert into activite (nom, fa_icone) values 
	('Equitation','horse'), 
	('Foot','futbol'),
	('Volley','volleyball-ball'),
	('Karaoke','music'),
	('Voile','anchor'),
	('Ping-pong','table-tennis'),
	('Randonnée','hiking'),
	('Vélo','bicycle'),
	('Mini-Club','child');

-- 7. reservables : hotels + stations
insert into reservable (description, nom, prix_par_jour, lieu_id, photo_principale) values 
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la ville. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hôtel de France',300,1,'https://img.freepik.com/photos-gratuite/carre-immeuble-ancien_1359-845.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la ville. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','OccitHotel',150,2,'https://img.freepik.com/photos-gratuite/vue-rue-ville-dans-nuit-valence-espagne_1398-4131.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la ville. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','NYC Hotel',500,3,'https://img.freepik.com/photos-gratuite/fond-rue-tokyo-moderne_23-2149394962.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la ville. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Le Montagnard',230,4,'https://img.freepik.com/photos-gratuite/vue-panoramique-du-lac-montagnes_1303-10572.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la ville. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel La Vista',450,5,'https://img.freepik.com/photos-gratuite/suite-chambre-coucher-moderne-classique-luxe-dans-hotel_105762-1787.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Cappacotel',320,6,'https://img.freepik.com/photos-gratuite/nature-vacances-detente-voyage-recours_1203-5032.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel du froid',420,7,'https://img.freepik.com/photos-gratuite/maison-au-bord-eau-plage-beau-soleil-couchant-horizon_181624-26906.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Very Cold Cabin',240,7,'https://img.freepik.com/photos-gratuite/petite-cabane-dans-champ_181624-883.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel So Nice',320,1,'https://img.freepik.com/photos-gratuite/grand-paysage-s-ouvre-derriere-table-diner-confortable_8353-93.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel Dubrovnik',520,8,'https://img.freepik.com/photos-gratuite/grand-paysage-s-ouvre-derriere-table-diner-confortable_8353-93.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Grand Hotel',620,9,'https://img.freepik.com/photos-gratuite/architecture-style-maroc_74190-6490.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hanoi Hotel',340,10,'https://img.freepik.com/photos-gratuite/belle-vue-architecture-coloree_181624-44665.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel Mihi Mona',720,9,'https://img.freepik.com/photos-gratuite/piscine-exterieure-dans-hotel-station_74190-2151.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Hotel So Nice',320,1,'https://img.freepik.com/photos-gratuite/grand-paysage-s-ouvre-derriere-table-diner-confortable_8353-93.jpg'),
	('Bienvenue à notre hôtel, un havre de luxe au cœur de la région. Découvrez des chambres élégantes offrant un confort inégalé, un restaurant primé proposant une cuisine exquise, et des installations haut de gamme telles que des piscines scintillantes et un centre de remise en forme moderne. Notre équipe dévouée est prête à vous offrir une expérience inoubliable, alliant service exceptionnel et élégance raffinée. Réservez dès maintenant pour vivre le summum du bien-être et du luxe lors de votre séjour.','Lux Hotel',520,4,'https://img.freepik.com/photos-gratuite/loisirs-jardin-magnifique-paysage-sante_1203-4856.jpg');

insert into hotel (etoiles, petit_dejeuner, reservable_id) values 
	(4,TRUE,1),
	(5,TRUE,2),
	(5,TRUE,3),
	(4,FALSE,4),
	(3,TRUE,5),
	(3,FALSE,6),
	(3,FALSE,7),
	(4,FALSE,8),
	(4,TRUE,9);
	

insert into station_balneaire (nb_piscine, nb_spa, reservable_id) values 
	(2,1,10),
	(3,2,11),
	(1,1,12),
	(1,2,13),
	(2,1,14),
	(2,1,15);

insert into join_reservable_activite (reservable_id, activite_id) values 
	(10, 2), (10, 3), (10, 5),
	(11, 1), (11, 2), (11, 5), (11, 7), (11, 9),
	(12, 1), (12, 3), (12, 6), (12, 7),
	(13, 1), (13, 7), (13, 8),
	(14, 4), (14, 8), (14, 9),
	(15, 2), (15, 4), (15, 7), (15, 9);

-- 8. trajets
insert into trajet (date_aller, date_retour, nb_personnes, destination_id, reservable_id) values 
	('2023-12-21 15:00:00','2023-12-26 20:00:00', 2, 1, 1),
	('2023-04-28 06:00:00','2023-05-07 21:00:00', 4, 2, 2),
	('2024-01-01 01:00:00','2024-01-15 13:00:00', 1, 3, 3),
	('2024-02-13 12:00:00','2024-02-16 16:00:00', 2, 4, 4),
	('2024-02-16 17:00:00','2024-02-20 09:00:00', 5, 5, 5),
	('2024-03-03 15:00:00','2024-03-20 06:00:00', 2, 6, 6),
	('2024-03-15 08:00:00','2024-03-28 07:00:00', 3, 7, 7),
	('2023-11-10 13:00:00','2023-11-20 03:00:00', 2, 7, 8),
	('2023-12-21 15:00:00','2023-12-26 20:00:00', 2, 1, 9),
	('2023-04-28 06:00:00','2023-05-07 21:00:00', 4, 8, 10 ),
	('2024-01-01 01:00:00','2024-01-15 13:00:00', 1, 9, 11),
	('2024-02-13 12:00:00','2024-02-16 16:00:00', 2, 10, 12),
	('2024-02-16 17:00:00','2024-02-20 09:00:00', 5, 9, 13),
	('2024-03-03 15:00:00','2024-03-20 06:00:00', 2, 1, 14),
	('2024-03-15 08:00:00','2024-03-28 07:00:00', 3, 4 , 15),
	('2023-11-10 13:00:00','2023-11-20 03:00:00', 2, 1, 1),
	('2023-08-10 13:00:00','2023-08-15 03:00:00', 1, 2, 2),
	('2024-01-10 15:00:00','2024-01-20 03:00:00', 6, 3, 3),
	('2023-07-10 13:00:00','2023-08-20 03:00:00', 5, 4, 4),
	('2024-06-10 16:00:00','2024-06-11 20:00:00', 2, 5, 5),
	('2024-05-15 12:00:00','2024-11-17 07:00:00', 4, 6, 6),
	('2023-10-10 13:00:00','2023-10-15 03:00:00', 2, 7, 7),
	('2024-08-01 13:00:00','2024-08-20 08:00:00', 6, 7, 8),
	('2024-03-15 20:00:00','2024-03-21 19:00:00', 2, 1, 9),
	('2023-04-14 06:00:00','2023-04-27 09:00:00', 1, 8, 10),
	('2024-12-10 23:00:00','2024-12-23 05:00:00', 3, 9, 11),
	('2023-09-10 06:00:00','2023-09-19 03:00:00', 4, 10, 12),
	('2024-11-10 13:00:00','2024-11-12 08:00:00', 1, 9, 13),
	('2023-07-10 05:00:00','2023-07-31 09:00:00', 6, 1, 14),
	('2024-06-12 06:00:00','2024-06-20 10:00:00', 2, 4, 15);

insert into commande (date_commande, is_active, panier_id, trajet_id) values
	('2024-01-01 01:00:00', FALSE, 1, 1),
	('2024-01-01 01:00:00', FALSE, 1, 2),
	('2024-01-01 01:00:00', FALSE, 1, 3),
	('2024-01-01 01:00:00', FALSE, 1, 4),
	('2024-01-01 01:00:00', FALSE, 1, 5),
	('2024-01-01 01:00:00', FALSE, 2, 6),
	('2024-01-01 01:00:00', FALSE, 2, 7),
	('2024-01-01 01:00:00', FALSE, 2, 8),
	('2024-01-01 01:00:00', FALSE, 2, 9),
	('2024-01-01 01:00:00', FALSE, 2, 10),
	('2024-01-01 01:00:00', FALSE, 2, 11),
	('2024-01-01 01:00:00', FALSE, 2, 12),
	('2024-01-01 01:00:00', FALSE, 2, 13),
	('2024-01-01 01:00:00', FALSE, 2, 14),
	('2024-01-01 01:00:00', FALSE, 2, 15),
	('2024-01-01 01:00:00', TRUE, 2, 16),
	('2024-01-01 01:00:00', TRUE, 2, 17),
	('2024-01-01 01:00:00', TRUE, 2, 18),
	('2024-01-01 01:00:00', TRUE, 2, 19),
	('2024-01-01 01:00:00', TRUE, 2, 20),
	('2024-01-01 01:00:00', TRUE, 1, 21),
	('2024-01-01 01:00:00', TRUE, 1, 22),
	('2024-01-01 01:00:00', TRUE, 1, 23),
	('2024-01-01 01:00:00', TRUE, 1, 24),
	('2024-01-01 01:00:00', FALSE, 1, 25),
	('2024-01-01 01:00:00', FALSE, 1, 26),
	('2024-01-01 01:00:00', FALSE, 1, 27),
	('2024-01-01 01:00:00', FALSE, 1, 28),
	('2024-01-01 01:00:00', FALSE, 1, 29),
	('2024-01-01 01:00:00', FALSE, 1, 30);

-- 9. avis

insert into avis (commentaire, date, note, user_id, reservable_id) values
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 2,1),   
	('Bel hotel. Cependant, quelques petits couacs tels que la qualité du service me pousse à mettre cette note. Le séjour était quand même très agréable', '2024-12-31 15:00:00', 3, 2,2),
	('Nous avons passé plusieurs nuits dans cet hôtel. Tout était super. Vous vous sentez chez vous. Tout le personnel est très sympathique et serviable.', '2024-12-31 15:00:00', 5, 3,3),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,4),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 2,5),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 3,6),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,7),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 5, 2,8),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,9),
	('Bel hotel. Cependant, quelques petits couacs tels que la qualité du service me pousse à mettre cette note. Le séjour était quand même très agréable', '2024-12-31 15:00:00', 3, 2,10),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 2,11),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,12),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,13),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,14),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,15),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,1),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 2,2),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,3),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 3,4),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,5),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 2,6),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 3,7),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,8),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,9),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 3,10),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,11),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 5, 2,12),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 6, 3,13),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00',4 , 2,14),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,15),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 5, 2,1),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,2),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 3,3),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,4),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,5),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 2,1),
	('Bel hotel. Cependant, quelques petits couacs tels que la qualité du service me pousse à mettre cette note. Le séjour était quand même très agréable', '2024-12-31 15:00:00', 3, 2,2),
	('Nous avons passé plusieurs nuits dans cet hôtel. Tout était super. Vous vous sentez chez vous. Tout le personnel est très sympathique et serviable.', '2024-12-31 15:00:00', 5, 3,3),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,4),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 2,5),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 3,6),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,7),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 5, 2,8),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,9),
	('Bel hotel. Cependant, quelques petits couacs tels que la qualité du service me pousse à mettre cette note. Le séjour était quand même très agréable', '2024-12-31 15:00:00', 3, 3,10),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 2,11),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,12),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 2,13),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,14),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,15),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,1),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 3,2),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,3),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 3,4),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 2,5),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 2,6),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 3,7),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 3,8),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,9),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 3,10),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 2,11),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 5, 2,12),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 6, 3,13),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00',4 , 3,14),
	('Très Bel hôtel, personnel pas avenant et nonchalant, le room service n’est pas disponible même si ils disent le contraire.', '2024-12-31 15:00:00', 3, 2,15),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 5, 2,1),
	('Très bon hotel , le personnel est extremement sympathique et serviable.Je conseille très fortement.', '2024-12-31 15:00:00', 5, 3,2),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 2,3),
	('Je ne recommande pas !! Personnel désagréable et impatient. Beau cadre tout de même', '2024-12-31 15:00:00', 2, 2,4),
	('Excellent hotel ! Je le recommande fortement ! Je reviendrais pour plusieurs jours.', '2024-12-31 15:00:00', 4, 3,5),
	('Très bonne literie !','2024-01-01 01:00:00',5,3,6),
	('Très bonne literie !','2024-01-01 01:00:00',5,2,7),
	('Buffet à volonté froid','2024-01-01 01:00:00',2,3,8),
	('Buffet à volonté froid','2024-01-01 01:00:00',2,2,9),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,2,10),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,3,11),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,3,12),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,2,12),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,2,13),
	('Très bon accueil, personnel aux petits soins','2024-01-01 01:00:00',5,3,14),
	('Énormément de poussière et d’acariens j’en suis allergique et je peux vous dire que j’ai eu des plaques !!!', '2024-12-31 15:00:00', 1, 3,15);
