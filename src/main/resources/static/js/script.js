//Activation tooltip bootstrap
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

/* LOGIN PAGE */

const TOGGLE = document.getElementById('toggle-password');
const PSWD = document.getElementById('password');
const InTOGGLE = document.getElementById('toggle-inputPassword');
const InPSWD = document.getElementById('inputPassword');



function togglePasswordVisibility(toggle, pswd) {
        const TYPE = pswd.getAttribute('type') === 'password' ? 'text' : 'password';
        pswd.setAttribute('type', TYPE);
        const EYES = toggle.getAttribute('class') === 'btn fas fa-eye' ? 'btn fas fa-eye-slash' : 'btn fas fa-eye';
        toggle.setAttribute('class', EYES);
};


/* Formulaire de trajet */

// Formulaire modale Back Office
$(document).on('click', '.modal-form', function () {
	const id = $(this).data('id')
	if (id !== undefined) {
		const fetchUrl = document.location + (document.location.toString().endsWith('/') ? '' : '/') + id
		$.ajax({
			url: document.location + (document.location.toString().endsWith('/') ? '' : '/') + $(this).data('id'),
			success: function (result) {
				for (const [key, value] of Object.entries(result)) {
					$('#' + key).val(value)
				}
			},
			error: () => $('#modal-form').map((i, form) => form.reset())
		})
	} else {
		$('#modal-form').map((i, form) => form.reset())
	}
	$('#modal-form').attr('action', $(this).data('submit'))
})


/* Page forgotten login */
const newPSWD = document.getElementById('inputPassword');
const confirmPSWD = document.getElementById('confirmPassword');
const message = document.getElementById('message');
const confirmTOGGLE = document.getElementById('toggle-confirmPassword');

function checkPassword() {
	if (newPSWD.value == confirmPSWD.value) {
    message.style.color = 'green';
    message.innerHTML = 'Mot de passe identique';
  } else {
    message.style.color = 'red';
    message.innerHTML = 'Mot de passe différent';
  }
}

//Notation en escargots
const color1 = '#dd7878'
const color2 = '#acb0be'

const snailRatings = document.getElementsByClassName('snail-rating')
for (const div of snailRatings) {
	const note = div.textContent
	div.innerText = ''
	for (let i = 0; i < 5; i++) {
		if (i <= note) {
			if (note < i+1) {
				// Fractional part 
				snailDiv(div, (note - i) * 100)
			} else {
			// Filled part
				snailDiv(div, 100)
			}
		} else {
			// Empty part
			snailDiv(div, 0)
		}
	}
}

function snailDiv(parent, percentage) {
	const maskDiv = document.createElement('div')
	maskDiv.classList.add('snail-mask')
	const child = document.createElement('div') 
	child.setAttribute('style', 'height: 100%; width: 100%; background: linear-gradient(to right, ' + color1 + ' ' + percentage + '%, ' + color2 + ' ' + percentage + '%)')
	maskDiv.appendChild(child)
	parent.appendChild(maskDiv)
}


//Caroussel Avis
document.getElementById("carousselCommentaire").children[0].children[0].classList.add("active");

//Check si c'est un hotel ou station balneaire show Banner()
function showBanner(id)
{ 
	var banner = document.getElementById("banner"+id);
	
	if (banner.style.display == "none"){
		banner.style.display = "flex";
	} else {
		banner.style.display = "none";
	}
	
}
