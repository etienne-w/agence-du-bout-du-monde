package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Reponse;

@Repository
public interface ReponseRepository extends JpaRepository<Reponse, Integer> {

}
