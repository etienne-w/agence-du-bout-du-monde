package fr.intiformation.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Destination;
import fr.intiformation.beans.Reservable;

@Repository
public interface ReservableRepository extends JpaRepository<Reservable, Integer> {

	public List<Reservable> findByLieu(Destination lieu);
	
	public Page<Reservable> findAll(Pageable pageable);
	
	public Page<Reservable> findByLieu(Pageable pageable, Destination lieu);
	
//	@Query(value = "select * from reservable where id>:idMin limit :nbVoulu", nativeQuery = true)
//	public List<Reservable> findAllReservableByPage (@Param("idMin") int idMin, @Param("nbVoulu") int nbVoulu);
//	
//	@Query(value = "select * from reservable where id>:idMin, lieu_id=:idLieu limit :nbVoulu", nativeQuery = true)
//	public List<Reservable> findAllReservableByLieuByPage (@Param("idMin") int idMin, @Param("nbVoulu") int nbVoulu, @Param("idLieu") int idLieu);
}
