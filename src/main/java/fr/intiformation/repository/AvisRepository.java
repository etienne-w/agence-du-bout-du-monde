package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Avis;
@Repository
public interface AvisRepository extends JpaRepository<Avis, Integer> {

}
