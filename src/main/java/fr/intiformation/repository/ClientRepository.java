package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Client;
@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

}
