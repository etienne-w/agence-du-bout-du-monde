package fr.intiformation.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Role;
import java.util.List;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

	public Optional<Role> findByRoleName(String roleName);
}
