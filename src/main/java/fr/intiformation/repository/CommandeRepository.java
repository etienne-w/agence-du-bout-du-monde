package fr.intiformation.repository;
import java.util.Optional;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.intiformation.beans.Commande;
import fr.intiformation.beans.Trajet;
import fr.intiformation.beans.Panier;
@Repository
public interface CommandeRepository extends JpaRepository<Commande, Integer> {
	
	public Optional<Commande> findByTrajet(Trajet trajet);
	public List<Commande> findByPanier(Panier panier);
}
