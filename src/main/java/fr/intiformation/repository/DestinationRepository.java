package fr.intiformation.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Destination;

@Repository
public interface DestinationRepository extends JpaRepository<Destination, Integer> {

	public Optional<Destination> findByVilleIgnoreCase(String ville);
	
}
