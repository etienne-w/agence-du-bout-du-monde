package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Panier;

@Repository
public interface PanierRepository extends JpaRepository<Panier, Integer> {

}
