package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Activite;

@Repository
public interface ActiviteRepository extends JpaRepository<Activite, Integer> {
}
