package fr.intiformation.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.VIP;

@Repository
@Transactional
public interface VipRepository extends JpaRepository<VIP, Integer>{

	@Modifying
	@Query(value = "INSERT INTO vip (user_id, points) VALUES (?1, 0)", nativeQuery = true)
	public void promote(int id);

	@Modifying
	@Query(value = "DELETE FROM vip WHERE user_id = ?1", nativeQuery = true)
	public void demote(int id);

}
