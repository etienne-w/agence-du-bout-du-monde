package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Remise;

@Repository
public interface RemiseRepository extends JpaRepository<Remise, Integer>{

}
