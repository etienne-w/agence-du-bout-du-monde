package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.StationBalneaire;

@Repository
public interface StationBalneaireRepository extends JpaRepository<StationBalneaire, Integer>{

}
