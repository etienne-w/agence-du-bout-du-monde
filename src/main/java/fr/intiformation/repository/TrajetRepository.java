package fr.intiformation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.intiformation.beans.Trajet;

@Repository
public interface TrajetRepository extends JpaRepository<Trajet, Integer> {

}
