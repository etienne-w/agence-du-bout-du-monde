package fr.intiformation.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import fr.intiformation.beans.User;
import fr.intiformation.service.ClientServiceImpl;
import fr.intiformation.service.DestinationServiceImpl;
import fr.intiformation.service.VipServiceImpl;

@Component
public class GlobalInterceptor extends HandlerInterceptorAdapter {

	public static final String ALL_DESTINATIONS = "allDestinationsG";
	public static final String IS_CLIENT = "isClientG";
	public static final String IS_VIP = "isVipG";
	public static final String PANIER_ID = "panierIdG";

	@Autowired
	DestinationServiceImpl destinationService;
	@Autowired
	ClientServiceImpl clientService;
	@Autowired
	VipServiceImpl vipService;

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (modelAndView != null) {
			modelAndView.addObject(ALL_DESTINATIONS, destinationService.getAllDestination());

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null && !auth.getPrincipal().equals("anonymousUser")) {
				User user = (User) auth.getPrincipal();
				modelAndView.addObject(IS_CLIENT, clientService.isClient(user));
				modelAndView.addObject(IS_VIP, vipService.isVip(user));
				modelAndView.addObject(PANIER_ID,
						clientService.isClient(user)
								? clientService.getClientById(user.getId()).getPanier().getId()
								: null);
			}
		}
	}

}
