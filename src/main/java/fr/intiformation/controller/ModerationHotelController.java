package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.Hotel;
import fr.intiformation.service.DestinationServiceImpl;
import fr.intiformation.service.HotelServiceImpl;


@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
@RequestMapping("/gestion/hotel")
public class ModerationHotelController {

	@Autowired
	HotelServiceImpl hotelService;
	@Autowired
	DestinationServiceImpl destinationService;


	@GetMapping("")
	public String Hotels(Model model) {
		model.addAttribute("hotels", this.hotelService.getAllHotel());
		model.addAttribute("newHotel", new Hotel());
		model.addAttribute("allDestinations", destinationService.getAllDestination());
		return "gestion/hotels";
	}

	@PostMapping("add")
	public String addHotel(@Valid @ModelAttribute("hotel") Hotel hotel, BindingResult result) {
		if (result.hasErrors()) {
			return "add-hotel";
		}
		this.hotelService.ajouterHotel(hotel);
		return "redirect:/gestion/hotel";

	}	


	@PostMapping("update/{id}")
	public String updateHotel(@PathVariable("id") int id, @Valid Hotel hotel, BindingResult result,
			Model model) {
		if (result.hasErrors()) {

			return "update-hotel";
		}
		hotelService.modifierHotel(hotel);
		model.addAttribute("hotels", this.hotelService.getAllHotel());
		return "redirect:/gestion/hotel";
	}

	@GetMapping("{id}")
	@ResponseBody
	public Hotel getHotelById(@PathVariable("id") int id) {
		return hotelService.getHotelById(id);
	}

	@GetMapping("delete/{id}")
	public String deleteHotel(@PathVariable("id") int id, Model model) {
		hotelService.supprimerHotelById(id);
		model.addAttribute(hotelService.getAllHotel());
		return "redirect:/gestion/hotel";
	}

}