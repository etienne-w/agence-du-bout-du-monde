package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fr.intiformation.beans.Avis;
import fr.intiformation.beans.User;
import fr.intiformation.service.AvisServiceImpl;
import fr.intiformation.service.ReservableServiceImpl;

@Controller
@PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
@RequestMapping("/avis")
public class AvisController {
	
	@Autowired
	AvisServiceImpl avisService;
	
	@Autowired
	ReservableServiceImpl reservableService;
	
	@GetMapping("/form")
	public String showAddForm(Avis avis, Model model, @AuthenticationPrincipal User user) {
		model.addAttribute("listeReservables", reservableService.getAllReservable());
		if (user != null) {
			model.addAttribute("userId", user.getId());
		}
		return "form-avis";
	}
	
	@PostMapping("/add")
	public String motoAdd(@Valid Avis avis, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "form-avis";
		}
		Avis avisSaved = this.avisService.ajouterAvis(avis);
		return "redirect:/reserver/" + avisSaved.getReservable().getId();
	}

}
