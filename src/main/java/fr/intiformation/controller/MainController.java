package fr.intiformation.controller;

import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import fr.intiformation.beans.Client;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.Role;
import fr.intiformation.beans.User;
import fr.intiformation.beans.VIP;
import fr.intiformation.service.ClientServiceImpl;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.RoleServiceImpl;
import fr.intiformation.service.UserServiceImpl;
import fr.intiformation.service.VipServiceImpl;

import fr.intiformation.beans.Destination;
import fr.intiformation.service.DestinationServiceImpl;

@Controller
@RequestMapping
public class MainController {

	@Autowired
	ClientServiceImpl clientService;

	@Autowired
	VipServiceImpl vipService;

	@Autowired
	UserServiceImpl userService;

	@Autowired
	RoleServiceImpl roleService;

	@Autowired
	DestinationServiceImpl destinationService;

	@Autowired
	PanierServiceImpl panierService;

	@GetMapping
	public String homePage(Model model) {
		Pageable pageable = PageRequest.of(0, 3);
		Page<Destination> carousel1 = destinationService.getAllDestinationsPage(pageable);
		model.addAttribute("carouselPage1", carousel1.getContent());
		if (carousel1.hasNext()) {
			Page<Destination> carousel2 = destinationService.getAllDestinationsPage(carousel1.nextPageable());
			model.addAttribute("carouselPage2", carousel2.getContent());
			if (carousel2.hasNext()) {
				Page<Destination> carousel3 = destinationService.getAllDestinationsPage(carousel2.nextPageable());
				model.addAttribute("carouselPage3", carousel3.getContent());

			} else {
				model.addAttribute("carouselPage3", null);
			}

		} else {
			model.addAttribute("carouselPage2", null);
		}

		return "home";
	}

	@GetMapping("login")
	public String loginPage(Model model, @RequestParam(name = "redirect", required = false) String redirect) {
		model.addAttribute("user", new User());
		model.addAttribute("formAddress", redirect == null ? "login" : "login?redirect=" + redirect);
		model.addAttribute("newUser", new User());
		model.addAttribute("formSigninAddress", redirect == null ? "signin" : "signin?redirectSignin=" + redirect);
		return "login";
	}
	
	//possibilité de le mettre dans ClientController
	@PostMapping("signin")
	public String signInPage(Model model, @Valid User user, BindingResult result, @RequestParam(name = "redirectSignin", required = false) String redirectSignin) {
		if (result.hasErrors()) {
			return "login";
		}
		Client newClient = new Client(user);
		Panier newPanier = new Panier();
		Panier panierDB = panierService.ajouterPanier(newPanier);
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleService.getRoleByName("ROLE_CLIENT"));
		newClient.setRoles(roles);
		newClient.setPanier(panierDB);
		Client newClientDB = this.clientService.ajouterClient(newClient);
		userService.ajouterUser(newClientDB);
		if (redirectSignin != null) {
			return "redirect:/login?redirect="+redirectSignin;
		} else {
			return "redirect:/login";
		}
	}

	@GetMapping("forgotten")
	public String forgottenPasswordPage(Model model) {
		model.addAttribute("user", new User());
		return "forgotten";
	}

	@PostMapping("forgotten")
	public String updatePassword(@Valid User user, @RequestParam("email") String email, Model model) {
		User userToFind = userService.getUserByEmail(email);
		if (userToFind == null) {
			model.addAttribute("errorMessage", "Nous ne parvenons pas à trouver un compte pour cette adresse email.");
			return "forgotten";
		} else {
			userToFind.setPassword(user.getPassword());
			userService.modifierUser(userToFind);
		}
		return "redirect:/";
	}

	@GetMapping("logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@GetMapping("contact")
	public String contactPage(Model model) {
		return "contact";
	}

	@GetMapping("apropos")
	public String aproposPage(Model model) {
		return "apropos";
	}

	@GetMapping("confidentialite")
	public String confidentialitePage(Model model) {
		return "confidentialite";
	}

	@GetMapping("monprofil")
	public String profilePage(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		User userToShow = userService.getUserByEmail(auth.getName());
		model.addAttribute("user", userToShow);
		if (vipService.isVip(userToShow)) {
			VIP vip = vipService.getVipById(userToShow.getId());
			model.addAttribute("clientPanierId", "/panier/" + vip.getPanier().getId());
			model.addAttribute("nombrePoints", vip.getPoints());
		} else if (clientService.isClient(userToShow)) {
			Client client = clientService.getClientById(userToShow.getId());
			model.addAttribute("clientPanierId", "/panier/" + client.getPanier().getId());
			model.addAttribute("euroToPoint", VIP.EURO_TO_POINTS);
			model.addAttribute("pointToEuro", VIP.POINTS_TO_EUROS);
		} else {
			model.addAttribute("clientPanierId", "/panier/erreur/404");
		}
		return "monprofil";
	}

	@GetMapping("monprofil/{id}")
	@PreAuthorize("isAuthenticated()")
	public String formProfilePage(@PathVariable("id") int id, Model model, @AuthenticationPrincipal User user) {
		if (id == user.getId() || userService.isAdmin(user)) {
		model.addAttribute("listeRoles", roleService.getAllRoles());
		User userToUpdate = userService.getUserById(id);
		model.addAttribute("user", userToUpdate);
		return "form-profil";}
		return "erreur-403";
	}

	@PostMapping("monprofil/{id}")
	@PreAuthorize("isAuthenticated()")
	public String updateProfilePage(@PathVariable("id") int id, @Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "form-profil";
		}
		if (id == user.getId() || userService.isAdmin(user)) {
		if (clientService.isClient(user)) {
			Client client = new Client(user);
			Client fromDB = clientService.getClientById(user.getId());
			client.setPanier(fromDB.getPanier());
			client.setListeAvis(fromDB.getListeAvis());
			client.setRoles(fromDB.getRoles());
			userService.modifierUser(client);
		} else if (vipService.isVip(user)) {
			VIP vip = new VIP(user);
			VIP fromDB = vipService.getVipById(user.getId());
			vip.setPanier(fromDB.getPanier());
			vip.setListeAvis(fromDB.getListeAvis());
			vip.setPoints(fromDB.getPoints());
			vip.setRoles(fromDB.getRoles());
			vip.setId(id);
			userService.modifierUser(vip);
		} else {
			user.setId(id);
			user.setRoles(user.getRoles());
			userService.modifierUser(user);
		}
		return "redirect:/monprofil";}
		return "erreur-403";
	}

	@GetMapping("monprofil/promote")
	public String promotionVip(@AuthenticationPrincipal User user) {
		if (clientService.isClient(user) && !vipService.isVip(user)) {
			vipService.promote(clientService.getClientById(user.getId()));
		}
		return "redirect:/monprofil";
	}

	@GetMapping("monprofil/demote")
	public String arretVip(@AuthenticationPrincipal User user) {
		if (vipService.isVip(user)) {
			vipService.demote(vipService.getVipById(user.getId()));
		}
		return "redirect:/monprofil";
	}

	@GetMapping("erreur/403")
	public String accessDeniedPage() {
		return "erreur-403";
	}

}
