package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.intiformation.beans.Commande;
import fr.intiformation.beans.Trajet;
import fr.intiformation.service.CommandeServiceImpl;
import fr.intiformation.service.DestinationServiceImpl;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.ReservableServiceImpl;
import fr.intiformation.service.TrajetServiceImpl;

@Controller
@RequestMapping("/trajet")
public class TrajetController {
	
	@Autowired
	TrajetServiceImpl trajetService;
	@Autowired
	CommandeServiceImpl commandeService;
	@Autowired
	PanierServiceImpl panierService;
	@Autowired
	ReservableServiceImpl reservableService;
	@Autowired
	DestinationServiceImpl destinationService;
	
	//afficher le formulaire
	@GetMapping("ShowForm")
	public String showTrajetForm(Trajet trajet, Model model ) {
		model.addAttribute("allDestinations", destinationService.getAllDestination());
		model.addAttribute("allReservables", reservableService.getAllReservable());
		return "new-trajet";
	}
	//crééer un trajet
	@PostMapping("add")
	public String addTrajet(@Valid Trajet trajet, BindingResult result, Model model) {
		if(result.hasErrors()) {
			model.addAttribute("allDestinations", destinationService.getAllDestination());
			model.addAttribute("allReservables", reservableService.getAllReservable());
			return "new-trajet";
		}
		trajet = this.trajetService.ajouterTrajet(trajet);
		Commande commande = new Commande();
		commande.setTrajet(trajet);
		commande = commandeService.ajouterCommande(commande);
		
		return "redirect:/commande/" + commande.getId();
	}
	
	//consulter un trajet
	@GetMapping("{id}")
	public String trajetPage(@PathVariable ("id") int id, Model model) {
		Trajet trajetChoisis = this.trajetService.getTrajetById(id);
		model.addAttribute(trajetChoisis);
		
		model.addAttribute("allDestinations", destinationService.getAllDestination());
		model.addAttribute("allReservables", reservableService.getAllReservable());
		return "editer-trajet";
		
	}
	//modifier un trajet
	@PostMapping("{id}")
	public String editTrajet(@PathVariable("id") int id, Trajet trajet) {
		
		int commandeId =commandeService.getByTrajet(trajetService.getTrajetById(id)).getId();
		trajet.setId(id);
		trajetService.modifierTrajet(trajet);
		
		return "redirect:/commande/" + commandeId;
	}
	

	
	
}
