package fr.intiformation.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.Commande;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.User;
import fr.intiformation.service.ClientServiceImpl;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.VipServiceImpl;


@Controller
@RequestMapping("/panier")
public class PanierController {
	
	@Autowired
	PanierServiceImpl panierService;
	@Autowired
	VipServiceImpl vipService;
	@Autowired
	ClientServiceImpl clientService;
	
	//consulter un panier
	@GetMapping("{id}")
	@PreAuthorize("isAuthenticated()")
	public String panierPage(@PathVariable ("id") int id, Model model, @AuthenticationPrincipal User user) {
		Client client = clientService.getClientById(user.getId());
		if (id == client.getPanier().getId()) {
			Panier PanierChoisis = this.panierService.getPanierById(id);
			boolean isVip = vipService.isVip(PanierChoisis.getClient());
			List<Commande> listCommandeActive = new ArrayList<>();
			List<Commande> listCommandeInactive = new ArrayList<>();
			for (Commande commande : PanierChoisis.getCommandes()) {
				if (commande.getIsActive()) {
					listCommandeActive.add(commande);
				}else {
					listCommandeInactive.add(commande);
				}
			}
			model.addAttribute("commandesActives", listCommandeActive);
			model.addAttribute("commandesInactives", listCommandeInactive);
			model.addAttribute("panierId", PanierChoisis.getId());
			model.addAttribute("isVip", isVip);
			if (isVip) {
				model.addAttribute("hasPoints", vipService.getVipById(PanierChoisis.getClient().getId()).getPoints() > 0);
			}
			return "consulter-panier";
		}
		return "erreur-403";
	}

	@GetMapping("erreur/404")
	public String notFoundPanierPage(Model model) {
		model.addAttribute("errorMessage", "Vous n'avez pas de panier.");
		return "erreur-404";
	}

	

	
	
}
