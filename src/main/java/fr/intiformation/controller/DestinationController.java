package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.Destination;
import fr.intiformation.service.DestinationServiceImpl;

@Controller
@RequestMapping("/gestion/destination")
@PreAuthorize(value = "hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
public class DestinationController {
	
	@Autowired
	DestinationServiceImpl destinationService;
	
	@GetMapping
	public String destinationPage (Model model) {
		model.addAttribute("destinations",destinationService.getAllDestination());
		model.addAttribute("newDestination", new Destination());
		return "/gestion/destinations";
	}
	
	
	@PostMapping("add")
	public String addDestination(@Valid Destination destination, BindingResult result, Model model ) {
		if (result.hasErrors()) {
			return "redirect:/gestion/destination";
		}
		this.destinationService.ajouterDestination(destination);
		return "redirect:/gestion/destination";
	}
		
	
	@PostMapping("update/{id}")
	public String updateDestination(@PathVariable("id") int id,@Valid Destination destination, BindingResult result, Model model ) {
		if (result.hasErrors()) {
			return "redirect:/gestion/destination";
		}
		destinationService.modifierDestination(destination);
		return "redirect:/gestion/destination";
	}
	
	@GetMapping("delete/{id}")
	public String deleteDestination(@PathVariable("id") int id, Model model) {
		Destination destinationaSupprimer = this.destinationService.getDestinationById(id);
		destinationService.supprimerDestination(destinationaSupprimer);
		return "redirect:/gestion/destination";
	}
	
	@GetMapping("{id}")
	@ResponseBody
	public Destination getById(@PathVariable("id") int id) {
		return destinationService.getDestinationById(id);
	}
		 
}
