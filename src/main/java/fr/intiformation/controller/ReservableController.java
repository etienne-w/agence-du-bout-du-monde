package fr.intiformation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.intiformation.beans.Destination;
import fr.intiformation.beans.Reservable;
import fr.intiformation.service.DestinationServiceImpl;
import fr.intiformation.service.HotelServiceImpl;
import fr.intiformation.service.ReservableServiceImpl;
import fr.intiformation.service.StationBalneaireServiceImpl;

@Controller
@RequestMapping("/reserver")
public class ReservableController {

	@Autowired
	ReservableServiceImpl reservableService;

	@Autowired
	DestinationServiceImpl destinationService;
	
	@Autowired
	HotelServiceImpl hotelService;
	
	@Autowired
	StationBalneaireServiceImpl stationBalneaireService;


	
	
	
	@GetMapping("")
	public String listReservable(Model model,
	        @RequestParam(name = "destination", required = false) String ville,
	        @RequestParam(name = "page", required = false, defaultValue = "1") int page,
	        @RequestParam(name = "pageSize", required = false, defaultValue = "5") int pageSize) {
		Pageable paginate = PageRequest.of(page - 1, pageSize);
	    Page<Reservable> listeReservables = null;
	    if (ville == null || ville.isEmpty()) {
	        listeReservables = reservableService.getAllReservablesPage(paginate);
	    } else {
	        Destination destination = destinationService.getDestinationByVille(ville);
	        listeReservables = reservableService.getByLieuPage(paginate, destination);
	        model.addAttribute("destination",destination);
	    }
	    model.addAttribute("reservables", listeReservables.getContent());
			model.addAttribute("destination", ville);
	    model.addAttribute("currentPage", page);
	    model.addAttribute("pageSize", pageSize);
	    model.addAttribute("itemsNumber", listeReservables.getTotalElements());
	    model.addAttribute("pageNumber", listeReservables.getTotalPages());
	    model.addAttribute("isLastPage", !listeReservables.hasNext());
	    model.addAttribute("isFirstPage", !listeReservables.hasPrevious());
	    return "search";
	}

	@GetMapping("{id}")
	public String reservablePage(Model model, @PathVariable("id") int id) {
		model.addAttribute("reservables", reservableService.getReservableById(id));
		Reservable reservable = reservableService.getReservableById(id);
		if(hotelService.isReservableHotel(reservable)) {
			model.addAttribute("hotel",hotelService.getHotelById(id));
			return "reservable-hotel";
		}
		if(stationBalneaireService.isReservableStationBalneaire(reservable)) {
			model.addAttribute("stationBalneaire", stationBalneaireService.getStationBalneaireById(id));
			return "reservable-station-balneaire";
		}
		return "erreur-404";
		
		
	}
	
	

}
