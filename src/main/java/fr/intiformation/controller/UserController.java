package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.User;
import fr.intiformation.service.RoleServiceImpl;
import fr.intiformation.service.UserServiceImpl;

@Controller
@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
@RequestMapping("/gestion/utilisateurs")
public class UserController {

	@Autowired
	UserServiceImpl userService;
	@Autowired
	RoleServiceImpl roleService;

	@GetMapping
	public String userPage(Model model) {
		model.addAttribute("Users", userService.getAllUsers());
		model.addAttribute("NewUser", new User());
		model.addAttribute("allRoles", roleService.getAllRoles());
		return "gestion/utilisateurs";
	}

	@PostMapping("add")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/utilisateurs";
		}
		this.userService.ajouterUser(user);
		return "redirect:/gestion/utilisateurs";

	}

	@PostMapping("update/{id}")
	public String updateUser(@PathVariable("id") int id, @Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/utilisateurs";
		}
		userService.modifierUser(user);
		return "redirect:/gestion/utilisateurs";
	}

	@GetMapping("delete/{id}")
	public String deleteUser(@PathVariable("id") int id, Model model) {
		User userASupprimer = this.userService.getUserById(id);
		userService.supprimerUser(userASupprimer);
		return "redirect:/gestion/utilisateurs";
	}

	@GetMapping("{id}")
	@ResponseBody
	public User getById(@PathVariable("id") int id) {
		return userService.getUserById(id);
	}

}
