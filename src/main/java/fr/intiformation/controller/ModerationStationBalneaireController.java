package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.StationBalneaire;
import fr.intiformation.service.ActiviteServiceImpl;
import fr.intiformation.service.DestinationServiceImpl;
import fr.intiformation.service.StationBalneaireServiceImpl;



@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
@RequestMapping("/gestion/stationbalneaire")
public class ModerationStationBalneaireController {

	@Autowired
	StationBalneaireServiceImpl stationBalneaireService;
	@Autowired
	DestinationServiceImpl destinationService;
	@Autowired
	ActiviteServiceImpl activiteService;


	@GetMapping("")
	public String stationBalneaires(Model model) {
		model.addAttribute("stationBalneaires", this.stationBalneaireService.getAllStationsBalneaires());
		model.addAttribute("newStationBalneaire", new StationBalneaire());
		model.addAttribute("allDestinations", destinationService.getAllDestination());
		model.addAttribute("allActivites", activiteService.getAllActivites());
		return "gestion/stationbalneaires";
	}

	@PostMapping("add")
	public String addStationBalneaire(@Valid @ModelAttribute("stationBalneaire") StationBalneaire stationBalneaire, BindingResult result) {
		if (result.hasErrors()) {
			return "add-stationbalneaire";
		}
		this.stationBalneaireService.ajouterStationBalneaire(stationBalneaire);
		return "redirect:/gestion/stationbalneaire";

	}	


	@PostMapping("update/{id}")
	public String updateStationBalneaire(@PathVariable("id") int id, @Valid StationBalneaire stationBalneaire, BindingResult result,
			Model model) {
//		if (result.hasErrors()) {
//
//			return "update-stationbalneaire";
//		}
		
		stationBalneaireService.modifierStationBalneaire(stationBalneaire);
		model.addAttribute("stationBalneaires", this.stationBalneaireService.getAllStationsBalneaires());
		return "redirect:/gestion/stationbalneaire";
	}

	@GetMapping("{id}")
	@ResponseBody
	public StationBalneaire getStationBalneaireById(@PathVariable("id") int id) {
		return stationBalneaireService.getStationBalneaireById(id);
	}

	@GetMapping("delete/{id}")
	public String deleteStationBalneaire(@PathVariable("id") int id, Model model) {
		stationBalneaireService.supprimerStationBalneaireById(id);
		model.addAttribute(stationBalneaireService.getAllStationsBalneaires());
		return "redirect:/gestion/stationbalneaire";
	}

}