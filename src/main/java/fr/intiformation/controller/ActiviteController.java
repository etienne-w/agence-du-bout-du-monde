package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.Activite;
import fr.intiformation.service.ActiviteServiceImpl;

@Controller
@RequestMapping("/gestion/activite")
@PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
public class ActiviteController {

	@Autowired
	ActiviteServiceImpl activiteService;

	// afficher la liste des activites
	@GetMapping("")
	public String showListeActivite(Model model) {
		model.addAttribute("activites", activiteService.getAllActivites());
		model.addAttribute("newActivite", new Activite());
		return "gestion/activites";
	}

	// créer une activite
	@PostMapping("add")
	public String addActivite(@Valid Activite activite, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/activite";
		}
		this.activiteService.ajouterActivite(activite);
		return "redirect:/gestion/activite";
	}

	// edit une activite
	@PostMapping("update/{id}")
	public String editActivite(@Valid Activite activite, @PathVariable("id") int id, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/activite";
		}
		this.activiteService.modifierActivite(activite);
		return "redirect:/gestion/activite";
	}
	
	//supprimer une activité
	@GetMapping("delete/{id}")
	public String deleteActivite(@PathVariable("id") int id) {
		this.activiteService.supprimerActiviteById(id);
		return "redirect:/gestion/activite";
	}

	@GetMapping("{id}")
	@ResponseBody
	public Activite getById(@PathVariable("id") int id) {
	return activiteService.getActiviteById(id);
	}

	
}
