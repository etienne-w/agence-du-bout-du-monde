package fr.intiformation.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.Role;
import fr.intiformation.beans.User;
import fr.intiformation.service.ClientServiceImpl;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.RoleServiceImpl;
import fr.intiformation.service.UserServiceImpl;

@Controller
@RequestMapping("/gestion/clients")
public class ClientController {

	@Autowired
	ClientServiceImpl clientService;
	@Autowired
	RoleServiceImpl roleService;
	@Autowired
	PanierServiceImpl panierService;
	@Autowired
	UserServiceImpl userService;
	
	@GetMapping
	public String clientPage(Model model) {
		model.addAttribute("Clients", clientService.getAllClient());
		model.addAttribute("NewClient", new Client());
		model.addAttribute("NewUser", new User());
		model.addAttribute("allRoles", roleService.getAllRoles());
		return "gestion/clients";
	}

	@PostMapping("add")
	public String addClient(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "gestion/clients";
		}
		Client newClient = new Client(user);
		Panier newPanier = new Panier();
		Panier panierDB = panierService.ajouterPanier(newPanier);
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleService.getRoleByName("ROLE_CLIENT"));
		newClient.setRoles(roles);
		newClient.setPanier(panierDB);
		Client newClientDB = this.clientService.ajouterClient(newClient);
		userService.ajouterUser(newClientDB);
		
		return "redirect:/gestion/clients";
		
	}

	@PostMapping("update/{id}")
	public String updateClient(@PathVariable("id") int id, @Valid Client client, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/clients";
		}
		clientService.ajouterClient(client);
		
		return "redirect:/gestion/clients";
	}

	@GetMapping("delete/{id}")
	public String deleteClient(@PathVariable("id") int id, Model model) {
		Client clientASupprimer = this.clientService.getClientById(id);
		clientService.supprimerClient(clientASupprimer);
		return "redirect:/gestion/clients";
	}

	@GetMapping("{id}")
	@ResponseBody
	public Client getById(@PathVariable("id") int id) {
		return clientService.getClientById(id);
	}

}
