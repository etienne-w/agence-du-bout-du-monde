package fr.intiformation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.Commande;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.Remise;
import fr.intiformation.beans.User;
import fr.intiformation.beans.VIP;
import fr.intiformation.service.ClientServiceImpl;
import fr.intiformation.service.CommandeServiceImpl;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.RemiseServiceImpl;
import fr.intiformation.service.VipServiceImpl;

@Controller
@RequestMapping("/commande")
public class CommandeController {

	@Autowired
	CommandeServiceImpl commandeService;

	@Autowired
	PanierServiceImpl panierService;

	@Autowired
	RemiseServiceImpl remiseService;

	@Autowired
	ClientServiceImpl clientService;

	@Autowired
	VipServiceImpl vipService;

	@GetMapping("{id}")
	public String commandeEditPage(@PathVariable("id") int id, Model model, RedirectAttributes redirectAttribute) {

		Commande commandeAModifier = this.commandeService.getCommandeById(id);

		redirectAttribute.addFlashAttribute("commandes", id);

		model.addAttribute(commandeAModifier);
		return "commande";
	}

	@PostMapping("{id}")
	public String commandeEdit(@PathVariable("id") int id, Commande commande) {
		commande.setId(id);
		commandeService.modifierCommande(commande);
		int panierid = commande.getPanier().getId();
		return "redirect:/panier/" + panierid;
	}

	@GetMapping("delete/{id}")
	public String deleteCommande(@PathVariable("id") int id, Model model) {
		Commande commandASupprimer = this.commandeService.getCommandeById(id);
		commandeService.supprimerCommande(commandASupprimer);
		model.addAttribute("commande", commandeService.getAllCommandes());
		int panierid = commandASupprimer.getPanier().getId();
		return "redirect:/panier/" + panierid;
	}

	@GetMapping("payer/{id}")
	public String payerCommande(@PathVariable("id") int id, Model model) {
		Commande commandAPayer = this.commandeService.getCommandeById(id);
		commandeService.payerCommande(commandAPayer);
		int panierid = commandAPayer.getPanier().getId();
		return "redirect:/panier/" + panierid;
	}

	@GetMapping("payerAll/{id}")
	public String toutPayerCommande(Model model, @PathVariable("id") int id) {
		Panier panierClient = panierService.getPanierById(id);
		List<Commande> commandes = commandeService.getCommandeByPanier(panierClient);
		for (Commande commande : commandes) {
			if (commande.getIsActive()) {
				commandeService.payerCommande(commande);
			}
		}
		return "redirect:/panier/" + id;
	}

	@GetMapping("remise/{id}")
	public String reductionCommande(@PathVariable("id") int id, Model model) {
		Commande commandAReduire = this.commandeService.getCommandeById(id);
		commandeService.reduireCommande(commandAReduire);
		int panierid = commandAReduire.getPanier().getId();
		return "redirect:/panier/" + panierid;
	}

	@GetMapping("ajouterPanier/{id}")
	public String ajouterAuPanier(@PathVariable("id") int id, Model model) {
		Commande commandAAjouter = this.commandeService.getCommandeById(id);
		commandAAjouter.setIsActive(true);
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Client client = clientService.getClientById(user.getId());
		commandAAjouter.setPanier(client.getPanier());
		commandeService.ajouterCommande(commandAAjouter);
		int panierid = commandAAjouter.getPanier().getId();
		return "redirect:/panier/" + panierid;
	}
}
