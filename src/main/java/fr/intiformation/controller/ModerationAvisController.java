package fr.intiformation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.intiformation.beans.Avis;
import fr.intiformation.beans.Reponse;
import fr.intiformation.service.AvisServiceImpl;
import fr.intiformation.service.ReponseServiceImpl;

@Controller
@PreAuthorize(value = "hasAnyRole('ROLE_EMPLOYEE','ROLE_ADMIN')")
@RequestMapping("/gestion/avis")
public class ModerationAvisController {
	
	@Autowired
	AvisServiceImpl avisService;
	
	@Autowired
	ReponseServiceImpl reponseService;
	
	@GetMapping("")
	public String moderationAvis(Model model, Reponse reponse) {
		model.addAttribute("avis", avisService.getAllAvis());
		return "gestion/avis";
	}
	
	@GetMapping("{id}")
	public String supprimerAvis(@PathVariable("id") int id, Model model) {
		avisService.supprimerAvisById(id);
		model.addAttribute("avis", avisService.getAllAvis());
		return "redirect:/gestion/avis";
	}

	@PostMapping("repondre/{id}")
	public String repondreAvis(@Valid Reponse reponse, BindingResult result, Model model, @PathVariable("id") int id) {
		Avis avis = avisService.getAvisById(id);
		if (result.hasErrors()) {
			return "modo-avis";
		}
		Reponse reponseToAdd = reponseService.ajouterReponse(reponse);
		avis.setReponse(reponseToAdd);
		return "redirect:/gestion/avis";
	}
}
