package fr.intiformation.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.Role;
import fr.intiformation.beans.User;
import fr.intiformation.beans.VIP;
import fr.intiformation.service.PanierServiceImpl;
import fr.intiformation.service.RoleServiceImpl;
import fr.intiformation.service.UserServiceImpl;
import fr.intiformation.service.VipServiceImpl;

@Controller
@RequestMapping("/gestion/Vips")
public class VIPController {

	@Autowired
	VipServiceImpl VipService;
	@Autowired
	RoleServiceImpl roleService;
	@Autowired
	PanierServiceImpl panierService;
	@Autowired 
	UserServiceImpl userService;

	@GetMapping
	public String VipPage(Model model) {
		model.addAttribute("Vips", VipService.getAllVips());
		model.addAttribute("NewVip", new VIP());
		model.addAttribute("NewUser", new User());
		model.addAttribute("allRoles", roleService.getAllRoles());
		return "gestion/Vips";
	}

	@PostMapping("add")
	public String addVip(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/Vips";
		}
		if (result.hasErrors()) {
			return "gestion/clients";
		}
		VIP newVip = new VIP(user);
		Panier newPanier = new Panier();
		Panier panierDB = panierService.ajouterPanier(newPanier);
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleService.getRoleByName("ROLE_CLIENT"));
		newVip.setRoles(roles);
		newVip.setPanier(panierDB);
		VIP newVipDB = this.VipService.ajouterVip(newVip);
		userService.ajouterUser(newVipDB);
		
		return "redirect:/gestion/Vips";

	}

	@PostMapping("update/{id}")
	public String updateVip(@PathVariable("id") int id, @Valid VIP Vip, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "redirect:/gestion/Vips";
		}
		VipService.ajouterVip(Vip);

		return "redirect:/gestion/Vips";
	}

	@GetMapping("delete/{id}")
	public String deleteVip(@PathVariable("id") int id, Model model) {
		VIP VipASupprimer = this.VipService.getVipById(id);
		VipService.supprimerVip(VipASupprimer);
		return "redirect:/gestion/Vips";
	}

	@GetMapping("{id}")
	@ResponseBody
	public VIP getById(@PathVariable("id") int id) {
		return VipService.getVipById(id);
	}

}
