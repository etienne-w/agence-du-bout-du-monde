package fr.intiformation.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

@Entity
@PrimaryKeyJoinColumn(name = "user_id", referencedColumnName = "id")
@Inheritance(strategy = InheritanceType.JOINED)
public class Client extends User {

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "panier_id", referencedColumnName = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private Panier panier;

	@OneToMany(mappedBy = "client", targetEntity = Avis.class, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Avis> listeAvis = new ArrayList<>();

	public Client() {
	}

	public Client(User user) {
		super(user);
	}

	public Client(Client client) {
		super(client);
		panier = client.getPanier();
		listeAvis = client.getListeAvis();
	}

	public Client(Panier panier, List<Avis> listeAvis) {

		this.panier = panier;
		this.listeAvis = listeAvis;
	}

	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	public List<Avis> getListeAvis() {
		return listeAvis;
	}

	public void setListeAvis(List<Avis> listeAvis) {
		this.listeAvis = listeAvis;
	}

}
