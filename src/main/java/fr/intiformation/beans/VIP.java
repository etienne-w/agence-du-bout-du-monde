package fr.intiformation.beans;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class VIP extends Client {

	@Transient
	public static final int EURO_TO_POINTS = 100;
	@Transient
	public static final int POINTS_TO_EUROS = 1;

	private int points;

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public VIP(int points) {
		super();
		this.points = points;
	}

	public VIP() {
		super();
	}

	public VIP(User user) {
		super(user);
	}

	public VIP(Client client) {
		super(client);
		points = 0;
	}

	@Override
	public String toString() {
		return "VIP [points=" + points + ", getPanier()=" + getPanier() + ", getListeAvis()=" + getListeAvis()
				+ ", getId()=" + getId() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom() + ", getEmail()="
				+ getEmail() + ", getPassword()=" + getPassword() + ", getRoles()=" + getRoles() + "]";
	}

	public Remise appliquerRemise(Commande commande) {
		Remise remise = new Remise();
		if (points * POINTS_TO_EUROS >= commande.total()) {
			int pointsDeduits = (int) commande.total() / POINTS_TO_EUROS;

			remise.setMontant(pointsDeduits * POINTS_TO_EUROS);
			points = points - pointsDeduits;
		} else {
			remise.setMontant(points * POINTS_TO_EUROS);
			points = 0;
		}
		return remise;
	}

}
