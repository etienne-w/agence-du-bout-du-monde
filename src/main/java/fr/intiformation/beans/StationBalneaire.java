package fr.intiformation.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

@Entity
@PrimaryKeyJoinColumn(name = "Reservable_ID", referencedColumnName = "id")
public class StationBalneaire extends Reservable {

	private int nbPiscine;
	private int nbSpa;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "join_reservable_activite", joinColumns = @JoinColumn(name = "reservable_id"), inverseJoinColumns = @JoinColumn(name = "activite_id"))
	@JsonIdentityReference(alwaysAsId = true)
	private List<Activite> activites;

	public StationBalneaire() {
		super();
	}

	public StationBalneaire(int nbPiscine, int nbSpa, List<Activite> activites) {
		super();
		this.nbPiscine = nbPiscine;
		this.nbSpa = nbSpa;
		this.activites = activites;
	}

	public int getNbPiscine() {
		return nbPiscine;
	}

	public void setNbPiscine(int nbPiscine) {
		this.nbPiscine = nbPiscine;
	}

	public int getNbSpa() {
		return nbSpa;
	}

	public void setNbSpa(int nbSpa) {
		this.nbSpa = nbSpa;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	@Override
	public String toString() {
		return "StationBalneaire [nbPiscine=" + nbPiscine + ", nbSpa=" + nbSpa + ", activites=" + activites
				+ ", getId()=" + getId() + ", getNom()=" + getNom() + ", getPrixParJour()=" + getPrixParJour()
				+ ", getDescription()=" + getDescription() + ", getLieu()=" + getLieu() + ", getAvis()=" + getAvis()
				+ ", getPhotoPrincipale()=" + getPhotoPrincipale() + "]";
	}

	

}
