package fr.intiformation.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Destination {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String ville;
	private double prix;
	@Length(max=1000)
	private String imageDestination;

	public Destination() {
	}

	public Destination(String ville, double prix) {
		this.ville = ville;
		this.prix = prix;
	}

	public Destination(String ville, double prix, String imageDestination) {
		super();
		this.ville = ville;
		this.prix = prix;
		this.imageDestination = imageDestination;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getImageDestination() {
		return imageDestination;
	}

	public void setImageDestination(String imageDestination) {
		this.imageDestination = imageDestination;
	}

	@Override
	public String toString() {
		return "Destination [id=" + id + ", ville=" + ville + ", prix=" + prix + "]";
	}

}
