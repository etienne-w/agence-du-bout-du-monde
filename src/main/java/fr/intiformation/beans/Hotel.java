package fr.intiformation.beans;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "reservable_id", referencedColumnName = "id")
public class Hotel extends Reservable {

	private int etoiles;
	private boolean petitDejeuner;

	public Hotel() {
	}

	public Hotel(String nom, double prixParJour, Destination destination, String description, int etoiles, boolean petitDejeuner) {
		super(nom, prixParJour, destination, description);
		this.etoiles = etoiles;
		this.petitDejeuner = petitDejeuner;
	}

	public int getEtoiles() {
		return etoiles;
	}

	public void setEtoiles(int etoiles) {
		this.etoiles = etoiles;
	}

	public boolean isPetitDejeuner() {
		return petitDejeuner;
	}

	public void setPetitDejeuner(boolean petitDejeuner) {
		this.petitDejeuner = petitDejeuner;
	}

	@Override
	public String toString() {
		return "Hotel [etoiles=" + etoiles + ", petitDejeuner=" + petitDejeuner + ", getId()=" + getId() + ", getNom()="
				+ getNom() + ", getPrixParJour()=" + getPrixParJour() + ", getDescription()=" + getDescription()
				+ ", getLieu()=" + getLieu() + ", getAvis()=" + getAvis() + ", getPhotoPrincipale()="
				+ getPhotoPrincipale() + "]";
	}

	
	
}
