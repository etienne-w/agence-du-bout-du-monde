package fr.intiformation.beans;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private LocalDateTime dateCommande;
	private boolean isActive;
	@ManyToOne
	@JoinColumn(name = "panier_id", referencedColumnName = "id")
	private Panier panier;
	@OneToOne(cascade = CascadeType.ALL)
	private Trajet trajet;
	@OneToOne(cascade = CascadeType.ALL)
	private Remise remise;

	public Commande() {
		dateCommande = LocalDateTime.now();
	}

	public Commande(Panier panier) {
		this();
		this.panier = panier;
	}

	public Commande(Panier panier, Trajet trajet, Remise remise) {
		this();
		this.panier = panier;
		this.trajet = trajet;
		this.remise = remise;
	}

	public Commande(LocalDateTime date, boolean active) {
		this.dateCommande = date;
		this.isActive = active;
	}

	public Commande(int id, LocalDateTime date, boolean active) {
		this.id = id;
		this.dateCommande = date;
		this.isActive = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(LocalDateTime date) {
		this.dateCommande = date;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean active) {
		this.isActive = active;
	}
	

	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	public Trajet getTrajet() {
		return trajet;
	}

	public void setTrajet(Trajet trajet) {
		this.trajet = trajet;
	}

	public Remise getRemise() {
		return remise;
	}

	public void setRemise(Remise remise) {
		this.remise = remise;
	}

	public double total() {
		return remise == null ? trajet.calculTotal() : trajet.calculTotal() + remise.total();
	}

	@Override
	public String toString() {
		return "Commande [id=" + id + ", total=" + total() + ", date=" + dateCommande + ", active=" + isActive + "]";
	}

}
