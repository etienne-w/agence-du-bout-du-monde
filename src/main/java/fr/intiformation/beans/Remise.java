package fr.intiformation.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Remise {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private double montant;

	public Remise() {
	}

	public Remise(double montant) {
		this.montant = montant;
	}

	public Remise(int id, double montant) {
		this.id = id;
		this.montant = montant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	@Override
	public String toString() {
		return "Remise [id=" + id + ", montant=" + montant + "]";
	}

	public double total() {
		return -montant;
	}
}
