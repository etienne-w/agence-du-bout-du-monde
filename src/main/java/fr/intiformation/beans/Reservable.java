package fr.intiformation.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Reservable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nom;
	private double prixParJour;
	@Length(max = 10000)
	private String description;

	@OneToOne
	@JoinColumn(name = "lieu_id", referencedColumnName = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private Destination lieu;

	@OneToMany(mappedBy = "reservable", targetEntity = Avis.class)
	@JsonIgnore
	private List<Avis> avis = new ArrayList<Avis>();
	
	@Length(max=1000)
	private String photoPrincipale;

	public Reservable() {
	}

	public Reservable(String nom, double prixParJour, Destination lieu) {
		this.nom = nom;
		this.prixParJour = prixParJour;
		this.lieu = lieu;
	}

	public Reservable(String nom, double prixParJour, Destination lieu, String description) {
		this.nom = nom;
		this.prixParJour = prixParJour;
		this.description = description;
		this.lieu = lieu;
	}
	
	

	public Reservable( String nom, double prixParJour, @Length(max = 10000) String description, Destination lieu,
			 String photoPrincipale) {
	
		this.nom = nom;
		this.prixParJour = prixParJour;
		this.description = description;
		this.lieu = lieu;
		this.photoPrincipale = photoPrincipale;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrixParJour() {
		return prixParJour;
	}

	public void setPrixParJour(double prixParJour) {
		this.prixParJour = prixParJour;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Destination getLieu() {
		return lieu;
	}

	public void setLieu(Destination lieu) {
		this.lieu = lieu;
	}

	public List<Avis> getAvis() {
		return avis;
	}

	public void setAvis(List<Avis> avis) {
		this.avis = avis;
	}
	
	public String getPhotoPrincipale() {
		return photoPrincipale;
	}

	public void setPhotoPrincipale(String photoPrincipale) {
		this.photoPrincipale = photoPrincipale;
	}

	

	public double noteMoyenne() {
		return avis.isEmpty() ? -1 : avis.stream().mapToInt(a -> a.getNote()).sum() / avis.size();
	}

	@Override
	public String toString() {
		return "Reservable [id=" + id + ", nom=" + nom + ", prixParJour=" + prixParJour + ", description=" + description
				+ ", lieu=" + lieu + ", avis=" + avis + ", photoPrincipale=" + photoPrincipale + "]";
	}



}
