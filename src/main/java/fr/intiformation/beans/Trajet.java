package fr.intiformation.beans;

import java.time.LocalDateTime;
import java.time.Period;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
public class Trajet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int nbPersonnes;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime dateAller;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime dateRetour;
	
	@ManyToOne
	@JoinColumn(name="destination_id",referencedColumnName = "id")
	private Destination destination;
	
	@OneToOne
	@JoinColumn(name="reservable_id",referencedColumnName = "id")
	private Reservable reservable;
	
	public Trajet() {
		
	}

	public Trajet(int nbPersonnes, LocalDateTime dateAller, LocalDateTime dateRetour) {

		this.nbPersonnes = nbPersonnes;
		this.dateAller = dateAller;
		this.dateRetour = dateRetour;
	}
	
	

	public Trajet(int id, int nbPersonnes, LocalDateTime dateAller, LocalDateTime dateRetour, Destination destination,
			Reservable reservable) {
		super();
		this.id = id;
		this.nbPersonnes = nbPersonnes;
		this.dateAller = dateAller;
		this.dateRetour = dateRetour;
		this.destination = destination;
		this.reservable = reservable;
	}
	
	
	public Trajet(LocalDateTime dateAller, LocalDateTime dateRetour, Destination destination, Reservable reservable) {
		super();
		this.dateAller = dateAller;
		this.dateRetour = dateRetour;
		this.destination = destination;
		this.reservable = reservable;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNbPersonnes() {
		return nbPersonnes;
	}

	public void setNbPersonnes(int nbPersonnes) {
		this.nbPersonnes = nbPersonnes;
	}

	public LocalDateTime getDateAller() {
		return dateAller;
	}

	public void setDateAller(LocalDateTime dateAller) {
		this.dateAller = dateAller;
	}

	public LocalDateTime getDateRetour() {
		return dateRetour;
	}

	public void setDateRetour(LocalDateTime dateRetour) {
		this.dateRetour = dateRetour;
	}
	

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public Reservable getReservable() {
		return reservable;
	}

	public void setReservable(Reservable reservable) {
		this.reservable = reservable;
	}

	@Override
	public String toString() {
		return "Trajet [id=" + id + ", nbPersonnes=" + nbPersonnes + ", dateAller=" + dateAller + ", dateRetour="
				+ dateRetour + ", destination=" + destination + ", reservable=" + reservable + "]";
	}
	
	public double calculTotal() {
		
	    int nb_nuit =  Period.between(dateAller.toLocalDate(), dateRetour.toLocalDate()).getDays();
	    
		return (nb_nuit*reservable.getPrixParJour() + destination.getPrix()* 2)* this.nbPersonnes ;
	}
	

}
