package fr.intiformation.beans;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.Length;

@Entity
public class Avis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int note;
	@Length(max = 10000)
	private String commentaire;
	private LocalDateTime date;
	
	@ManyToOne
	@JoinColumn(name ="user_id", referencedColumnName = "user_id")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name="reservable_id", referencedColumnName = "id")
	private Reservable reservable;
	
	@OneToOne(mappedBy ="avis")
	private Reponse reponse;

	public Avis() {
		date = LocalDateTime.now();
	}

	public Avis(int note, String commentaire, LocalDateTime date, Client client, Reservable reservable, Reponse reponse) {
		this.note = note;
		this.commentaire = commentaire;
		this.date = date;
		this.client = client;
		this.reservable = reservable;
		this.reponse = reponse;
	}

	public Avis(int id, int note, String commentaire, LocalDateTime date, Client client, Reservable reservable, Reponse reponse) {
		this.id = id;
		this.note = note;
		this.commentaire = commentaire;
		this.date = date;
		this.client = client;
		this.reservable = reservable;
		this.reponse = reponse;
	}

	public Avis(int note, String commentaire) {
		this();
		this.note = note;
		this.commentaire = commentaire;
	}

	public Avis(int note, String commentaire, LocalDateTime date,  Reponse reponse) {
		this.note = note;
		this.commentaire = commentaire;
		this.date = date;
		this.reponse = reponse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Reservable getReservable() {
		return reservable;
	}

	public void setReservable(Reservable reservable) {
		this.reservable = reservable;
	}
	
	public Reponse getReponse() {
		return reponse;
	}

	public void setReponse(Reponse reponse) {
		this.reponse = reponse;
	}

}
