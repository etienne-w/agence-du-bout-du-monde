package fr.intiformation.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.Length;

@Entity
public class Reponse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Length(max = 10000)
	private String message;
	
	@OneToOne
	@JoinColumn(name = "avis_id", referencedColumnName = "id")
	private Avis avis;

	public Reponse() {
	}

	public Reponse(int id, @Length(max = 10000) String message, Avis avis) {
		this.id = id;
		this.message = message;
		this.avis = avis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Avis getAvis() {
		return avis;
	}

	public void setAvis(Avis avis) {
		this.avis = avis;
	}

	@Override
	public String toString() {
		return "Reponse [id=" + id + ", message=" + message + ", avis=" + avis + "]";
	}

}
