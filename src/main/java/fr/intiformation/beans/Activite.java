package fr.intiformation.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Activite {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String faIcone;
	@ManyToMany(mappedBy = "activites")
	private List<StationBalneaire> stationBalneaires;

	public Activite() {
	}

	public Activite(String nom) {
		this.nom = nom;
	}

	public Activite(String nom, String faIcone) {
		this.nom = nom;
		this.faIcone = faIcone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getFaIcone() {
		return faIcone;
	}

	public void setFaIcone(String faIcone) {
		this.faIcone = faIcone;
	}

	public List<StationBalneaire> getStationBalneaires() {
		return stationBalneaires;
	}

	public void setStationBalneaires(List<StationBalneaire> reservables) {
		this.stationBalneaires = reservables;
	}

	@Override
	public String toString() {
		return "Activite [id=" + id + ", nom=" + nom + ", faIcone=" + faIcone+"]";
	}
	
}
