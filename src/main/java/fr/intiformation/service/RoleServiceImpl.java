package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Role;
import fr.intiformation.repository.RoleRepository;

@Service
public class RoleServiceImpl {
	
	@Autowired
	RoleRepository roleRepository;
	
	public Role ajouterRole(Role role) {
		return roleRepository.save(role);
	}

	public Role modifierRole (Role role) {
		return roleRepository.save(role);
	}
	
	public void supprimerRole (Role role) {
		roleRepository.delete(role);
	}
	
	public void supprimerRoleById (int id) {
		roleRepository.deleteById(id);
	}
	
	public List<Role> getAllRoles (){
		return roleRepository.findAll();
	}
	
	public Role getRoleById (int id) {
		return roleRepository.findById(id).get();
	}
	
	public Role getRoleByName (String roleName) {
		return roleRepository.findByRoleName(roleName).get();
	}
}
