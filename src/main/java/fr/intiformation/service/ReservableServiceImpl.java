package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Destination;
import fr.intiformation.beans.Reservable;
import fr.intiformation.repository.ReservableRepository;

@Service
public class ReservableServiceImpl {

	@Autowired
	ReservableRepository reservableRepository;

	public Reservable ajouterReservable(Reservable reservable) {
		return reservableRepository.save(reservable);
	}

	public Reservable modifierReservable(Reservable reservable) {
		return reservableRepository.save(reservable);
	}

	public void supprimerReservable(Reservable reservable) {
		reservableRepository.delete(reservable);
	}

	public void supprimerReservableById(int id) {
		reservableRepository.deleteById(id);
	}

	public List<Reservable> getAllReservable() {
		return reservableRepository.findAll();
	}

	public Reservable getReservableById(int id) {
		return reservableRepository.findById(id).get();
	}

	public List<Reservable> getByLieu(Destination lieu) {
		return reservableRepository.findByLieu(lieu);
	}

	public Page<Reservable> getAllReservablesPage(Pageable paginate) {
		return reservableRepository.findAll(paginate);
	}

	public Page<Reservable> getByLieuPage(Pageable paginate, Destination lieu) {
		return reservableRepository.findByLieu(paginate, lieu);
	}

//	public List<Reservable> getAllReservableByPage(int idMin, int nbVoulu){
//		return reservableRepository.findAllReservableByPage(idMin, nbVoulu);
//	}
//	
//	public List<Reservable> getAllReservableByPageByLieu(int idMin, int nbVoulu, Destination lieu){
//		int idLieu = lieu.getId();
//		return reservableRepository.findAllReservableByLieuByPage(idMin, nbVoulu, idLieu);
//	}

}
