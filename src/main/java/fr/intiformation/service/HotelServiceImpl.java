package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Hotel;
import fr.intiformation.beans.Reservable;
import fr.intiformation.repository.HotelRepository;

@Service
public class HotelServiceImpl {

	@Autowired
	HotelRepository hotelRepository;

	public Hotel ajouterHotel(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public Hotel modifierHotel(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public void supprimerHotel(Hotel hotel) {
		hotelRepository.delete(hotel);
	}

	public void supprimerHotelById(int id) {
		hotelRepository.deleteById(id);
	}

	public List<Hotel> getAllHotel() {
		return hotelRepository.findAll();
	}

	public Hotel getHotelById(int id) {
		return hotelRepository.findById(id).get();
	}

	public boolean isReservableHotel(Reservable r) {
		return hotelRepository.findById(r.getId()).isPresent();
	}

}
