package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Reponse;
import fr.intiformation.repository.ReponseRepository;

@Service
public class ReponseServiceImpl {
	
	@Autowired
	ReponseRepository reponseRepository;
	
		public Reponse ajouterReponse(Reponse reponse) {
		return reponseRepository.save(reponse);
	}
	
	public Reponse modifierReponse(Reponse reponse) {
		return reponseRepository.save(reponse);
	}
	
	public void supprimerReponse(Reponse reponse) {
		reponseRepository.delete(reponse);
	}
	
	public void supprimerReponseById(int id) {
		reponseRepository.deleteById(id);
	}
	
	public List<Reponse> getAllReponse(){
		return reponseRepository.findAll();
	}
	
	public Reponse getReponseById(int id) {
		return reponseRepository.findById(id).get();
	}

}
