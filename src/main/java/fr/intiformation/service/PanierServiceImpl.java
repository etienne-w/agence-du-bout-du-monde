package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Panier;
import fr.intiformation.repository.PanierRepository;

@Service
public class PanierServiceImpl {

	@Autowired
	PanierRepository panierRepository;
	
	public Panier ajouterPanier(Panier panier) {
		return panierRepository.save(panier);
	}
	
	public Panier modifierPanier (Panier panier) {
		return panierRepository.save(panier);
	}
	
	public void supprimerPanier (Panier panier) {
		panierRepository.delete(panier);
	}
	
	public void supprimerPanierById (int id) {
		panierRepository.deleteById(id);
	}
	
	public Panier getPanierById (int id) {
		return panierRepository.findById(id).get();
	}
	
	public List<Panier> getAllPaniers (){
		return panierRepository.findAll();
	}
	
	
	
	
}
