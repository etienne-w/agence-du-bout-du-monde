package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Activite;
import fr.intiformation.repository.ActiviteRepository;

@Service
public class ActiviteServiceImpl {

	@Autowired
	ActiviteRepository activiteRepository;

	public Activite ajouterActivite(Activite activite) {
		return activiteRepository.save(activite);
	}

	public Activite modifierActivite(Activite activite) {
		return activiteRepository.save(activite);
	}

	public void supprimerActivite(Activite activite) {
		activiteRepository.delete(activite);
	}

	public void supprimerActiviteById(int id) {
		activiteRepository.deleteById(id);
	}

	public List<Activite> getAllActivites() {
		return activiteRepository.findAll();
	}

	public Activite getActiviteById(int id) {
		return activiteRepository.findById(id).get();
	}
}
