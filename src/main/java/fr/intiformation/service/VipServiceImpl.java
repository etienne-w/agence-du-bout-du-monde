package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.User;
import fr.intiformation.beans.VIP;
import fr.intiformation.repository.VipRepository;

@Service
public class VipServiceImpl {

	@Autowired
	VipRepository vipRepository;
	@Autowired
	PasswordEncoder encoder;
	@Autowired
	ClientServiceImpl clientService;
	
	public VIP ajouterVip (VIP vip) {
		return vipRepository.save(vip);
	}
	
	public VIP modifierVip (VIP vip) {
		if (vip.getPassword().isEmpty()) {
			vip.setPassword(getVipById(vip.getId()).getPassword());
		} else {
			vip.setPassword(encoder.encode(vip.getPassword()));
		}
		return vipRepository.save(vip);
	}
	
	public void supprimerVip (VIP vip) {
		vipRepository.delete(vip);
	}
	
	public void supprimerVipById (int id) {
		vipRepository.deleteById(id);
	}
	
	public List<VIP> getAllVips(){
		return vipRepository.findAll();
	}
	
	public VIP getVipById(int id) {
		return vipRepository.findById(id).get();
	}

	public boolean isVip(User user) {
		return vipRepository.findById(user.getId()).isPresent();
	}

	public VIP promote(Client client) {
		int id = client.getId();
		if (!isVip(client)) {
			vipRepository.promote(id);
		}
		return new VIP(client);
	}

	public Client demote(VIP vip) {
		int id = vip.getId();
		vipRepository.demote(id);
		return vip;
	}
}
