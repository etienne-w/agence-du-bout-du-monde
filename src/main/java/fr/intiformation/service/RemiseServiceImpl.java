package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Remise;
import fr.intiformation.repository.RemiseRepository;

@Service
public class RemiseServiceImpl {
	
	@Autowired
	RemiseRepository remiseRepository;
	
	public Remise ajouterRemise(Remise remise) {
		return remiseRepository.save(remise);
	}
	
	public Remise modifierRemise(Remise remise) {
		return remiseRepository.save(remise);
	}
	
	public void supprimerRemise(Remise remise) {
		remiseRepository.delete(remise);
	}
	
	public void supprimerRemiseById(int id) {
		remiseRepository.deleteById(id);
	}
	
	public List<Remise> getAllRemise() {
		return remiseRepository.findAll();
	}
	
	public Remise getRemiseById(int id) {
		return remiseRepository.findById(id).get();
	}

}
