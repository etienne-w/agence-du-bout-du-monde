package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.Commande;
import fr.intiformation.beans.Trajet;
import fr.intiformation.beans.VIP;
import fr.intiformation.beans.Panier;
import fr.intiformation.beans.Remise;
import fr.intiformation.repository.CommandeRepository;

@Service
public class CommandeServiceImpl {

	@Autowired
	CommandeRepository commandeRepo;
	@Autowired
	RemiseServiceImpl remiseService;
	@Autowired
	VipServiceImpl vipService;

	public Commande ajouterCommande(Commande commande) {
		return commandeRepo.save(commande);
	}

	public Commande modifierCommande(Commande commande) {
		return commandeRepo.save(commande);
	}

	public void supprimerCommande(Commande commande) {
		commandeRepo.delete(commande);
	}

	public void supprimerCommandeById(int id) {
		commandeRepo.deleteById(id);
	}

	public List<Commande> getAllCommandes() {
		return commandeRepo.findAll();
	}

	public Commande getCommandeById(int id) {
		return commandeRepo.findById(id).get();
	}

	public Commande getByTrajet(Trajet t) {
		return commandeRepo.findByTrajet(t).orElse(null);
	}

	public List<Commande> getCommandeByPanier(Panier panier) {
		return commandeRepo.findByPanier(panier);
	}

	public void payerCommande(Commande commande) {
		Client client = commande.getPanier().getClient();
		commande.setIsActive(false);
		if (vipService.isVip(client)) {
			VIP vip = vipService.getVipById(client.getId());
			vip.setPoints(vip.getPoints() + (int) commande.total() / VIP.EURO_TO_POINTS);
		}
		modifierCommande(commande);
	}

	public void reduireCommande(Commande commandAReduire) {
		VIP vip = vipService.getVipById(commandAReduire.getPanier().getClient().getId());
		Remise remise = remiseService.ajouterRemise(vip.appliquerRemise(commandAReduire));
		commandAReduire.setRemise(remise);
		modifierCommande(commandAReduire);
	}

}
