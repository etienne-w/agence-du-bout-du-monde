package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Client;
import fr.intiformation.beans.User;
import fr.intiformation.repository.ClientRepository;

@Service
public class ClientServiceImpl {
	
	@Autowired
	ClientRepository clientRepository;
	@Autowired
	PasswordEncoder encoder;

	public Client ajouterClient(Client client) {
		return clientRepository.save(client);
	}
	
	public Client modifierClient(Client client) {
		if (client.getPassword().isEmpty()) {
			client.setPassword(getClientById(client.getId()).getPassword());
		} else {
			client.setPassword(encoder.encode(client.getPassword()));
		}
		return clientRepository.save(client);
	}
	
	public void supprimerClient(Client client) {
		clientRepository.delete(client);
	}
	
	public void supprimerClientById(int id) {
		clientRepository.deleteById(id);
	}
	
	public List<Client> getAllClient() {
		return clientRepository.findAll();
	}
	
	public Client getClientById(int id) {
		return clientRepository.findById(id).get();
	}

	public boolean isClient(User user) {
		return clientRepository.findById(user.getId()).isPresent();
	}
}
