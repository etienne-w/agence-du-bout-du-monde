package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Avis;
import fr.intiformation.repository.AvisRepository;

@Service
public class AvisServiceImpl {

	@Autowired
	AvisRepository avisRepository;
	
	public Avis ajouterAvis(Avis avis) {
		return avisRepository.save(avis);
	}
	
	public Avis modifierAvis(Avis avis) {
		return avisRepository.save(avis);
	}
	
	public void supprimerAvis(Avis avis) {
		avisRepository.delete(avis);
	}
	
	public void supprimerAvisById(int id) {
		avisRepository.deleteById(id);
	}
	
	public List<Avis> getAllAvis(){
		return avisRepository.findAll();
	}
	
	public Avis getAvisById(int id) {
		return avisRepository.findById(id).get();
	}
}