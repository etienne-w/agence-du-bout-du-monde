package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Trajet;
import fr.intiformation.repository.TrajetRepository;

@Service
public class TrajetServiceImpl {

	@Autowired
	TrajetRepository trajetRepository;
	
	public Trajet ajouterTrajet(Trajet trajet) {
		return trajetRepository.save(trajet);
	}
	
	public Trajet modifierTrajet (Trajet trajet) {
		return trajetRepository.save(trajet);
	}
	
	public void supprimerTrajet (Trajet trajet) {
		trajetRepository.delete(trajet);
	}
	
	public void supprimerTrajetById (int id) {
		trajetRepository.deleteById(id);
	}
	
	public List<Trajet> getAllTrajets(){
		return trajetRepository.findAll();
	}
	
	public Trajet getTrajetById(int id) {
		return trajetRepository.findById(id).get();
	}
	
}

