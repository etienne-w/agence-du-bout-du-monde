package fr.intiformation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Destination;
import fr.intiformation.repository.DestinationRepository;

@Service
public class DestinationServiceImpl {

	@Autowired
	DestinationRepository destinationRepository;

	public Destination ajouterDestination(Destination destination) {
		return destinationRepository.save(destination);
	}

	public Destination modifierDestination(Destination destination) {
		return destinationRepository.save(destination);
	}

	public void supprimerDestination(Destination destination) {
		destinationRepository.delete(destination);
	}

	public void supprimerDestinationById(int id) {
		destinationRepository.deleteById(id);
	}

	public List<Destination> getAllDestination() {
		return destinationRepository.findAll();
	}

	public Destination getDestinationById(int id) {
		return destinationRepository.findById(id).get();
	}

	public Destination getDestinationByVille(String ville) {
		return destinationRepository.findByVilleIgnoreCase(ville).orElse(null);

	}
	public Page<Destination> getAllDestinationsPage(Pageable pageable){
		return destinationRepository.findAll(pageable);
	}
}
