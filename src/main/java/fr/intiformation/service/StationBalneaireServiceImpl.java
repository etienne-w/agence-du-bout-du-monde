package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.Reservable;
import fr.intiformation.beans.StationBalneaire;
import fr.intiformation.repository.StationBalneaireRepository;

@Service
public class StationBalneaireServiceImpl {

	@Autowired
	StationBalneaireRepository sbRepository;

	public StationBalneaire ajouterStationBalneaire(StationBalneaire sb) {
		return sbRepository.save(sb);
	}

	public StationBalneaire modifierStationBalneaire(StationBalneaire sb) {
		return sbRepository.save(sb);
	}

	public void supprimerStationBalneaire(StationBalneaire sb) {
		sbRepository.delete(sb);
	}
	
	public void supprimerStationBalneaireById(int id) {
		sbRepository.deleteById(id);
	}

	public List<StationBalneaire> getAllStationsBalneaires() {
		return sbRepository.findAll();
	}

	public StationBalneaire getStationBalneaireById(int id) {
		return sbRepository.findById(id).get();
	}
	public boolean isReservableStationBalneaire(Reservable r) {
		return sbRepository.findById(r.getId()).isPresent();
	}
}
