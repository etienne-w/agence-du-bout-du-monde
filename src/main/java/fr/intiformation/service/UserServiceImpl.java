package fr.intiformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.intiformation.beans.User;
import fr.intiformation.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}


	public User ajouterUser(User user) {
		user.setPassword(encoder().encode(user.getPassword()));
		return userRepository.save(user);
	}

	public User modifierUser(User user) {
		if (user.getPassword().isEmpty()) {
			user.setPassword(getUserById(user.getId()).getPassword());
		} else {
			user.setPassword(encoder().encode(user.getPassword()));
		}
		return userRepository.save(user);
	}

	public void supprimerUser(User user) {
		userRepository.delete(user);
	}

	public void supprimerUserById(int id) {
		userRepository.deleteById(id);
	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public User getUserById(int id) {
		return userRepository.findById(id).get();
	}
	
	public User getUserByEmail(String email) {
		return userRepository.findByEmail(email).orElse(null);
	}
	



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username).get();
		if (user == null) {
			throw new UsernameNotFoundException("Utilisateur non trouvé");
		}
		return user;
	}

	public boolean isAdmin(User user) {
		return user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public boolean isEmployee(User user) {
		return user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_EMPLOYEE"));
	}

}
